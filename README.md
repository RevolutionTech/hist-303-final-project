# HIST 303 Final Project
## Music History and Data
#### By Lucas Connors

For my final project in HIST 303 (Digital History), I decided to collect data on the history of music and make observations on the history of music based on these data. I collected my data and MIDIs from [midiworld.com](http://www.midiworld.com) by scraping the site with [BeautifulSoup](http://www.crummy.com/software/BeautifulSoup/). This repo contains the scripts I used to generate and structure these data.

### Prerequisites

I recommend using a virtual environment for this project. If you don't have it already, you can install [virtualenv](http://virtualenv.readthedocs.org/en/latest/virtualenv.html) and virtualenvwrapper globally with pip:

    sudo pip install virtualenv virtualenvwrapper

[Update your .profile or .bashrc file](http://virtualenvwrapper.readthedocs.org/en/latest/install.html#shell-startup-file) to create new environment variables for virtualenvwrapper and then create and activate your virtual environment with:

    mkvirtualenv hist-303-final-project

In the future you can reactivate the virtual environment with:

    workon hist-303-final-project

### Installation

In your virtual environment, you will need to install requests, Unidecode, internetarchive, and beautifulsoup4:

    pip install -r requirements.txt

### Running

To begin, generate the report by running `python report.py`. This Python module will generate four files: `composers.csv`, `songs.csv`, `keys.csv`, and `timesigs.csv`. These four files contain the data from MidiWorld with respect to composers, music pieces, key signatures, and time signatures respectively.

### Further Analysis

To perform further analysis, I then created a SQL database from the raw CSV data and ran queries to acquire information in aggregate which then could be visualized in R. You may also find spreadsheet software, such as Excel helpful for analyzing the raw data.
