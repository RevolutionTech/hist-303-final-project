"""
:Created: 25 March 2015
:Author: Lucas Connors

Inspect data and MIDI files to put together report.csv

"""

import csv
import sys

from unidecode import unidecode

from midiworld import MidiWorld
from key import midi_keys
from timesig import midi_timesigs


class Report(object):

    def __init__(self, composerfilename=None, songfilename=None, keyfilename=None, timesigfilename=None):
        self.get_data()
        self.generate_report(songfilename=songfilename, keyfilename=keyfilename, timesigfilename=timesigfilename)

    def get_data(self):
        self.mw = MidiWorld()
        self.composers = self.mw.composers
        self.songs = self.mw.songs

    def generate_report(self, composerfilename=None, songfilename=None, keyfilename=None, timesigfilename=None):
        sys.stdout.write('Generating a report...')
        sys.stdout.flush()

        # Open files
        if composerfilename is None: composerfilename = 'composers.csv'
        if songfilename is None: songfilename = 'songs.csv'
        if keyfilename is None: keyfilename = 'keys.csv'
        if timesigfilename is None: timesigfilename = 'timesigs.csv'
        composerfile = open(composerfilename, 'wb')
        songfile = open(songfilename, 'wb')
        keyfile = open(keyfilename, 'wb')
        timesigfile = open(timesigfilename, 'wb')
        composerwriter = csv.writer(composerfile)
        songwriter = csv.writer(songfile)
        keywriter = csv.writer(keyfile)
        timesigwriter = csv.writer(timesigfile)

        # Write headers
        composerwriter.writerow(['Composer ID', 'Name', 'Nationality', 'Birth Year', 'Death Year'])
        songwriter.writerow(['Composer ID', 'Song ID', 'Song Title'])
        keywriter.writerow(['Song ID', 'Key Signature'])
        timesigwriter.writerow(['Song ID', 'Time Signature'])

        # Write data for each song
        i = j = 1
        for composer, song_list in self.songs.iteritems():
            composer_full_name = composer.first_name + ' ' + composer.last_name
            composerwriter.writerow([i, composer_full_name, composer.nationality, composer.birth_year, composer.death_year])

            for song in song_list:
                key_list = midi_keys(song.midi)
                timesig_list = midi_timesigs(song.midi)

                songwriter.writerow([i, j, unidecode(song.title)])
                for key in key_list: keywriter.writerow([j, key])
                for timesig in timesig_list: timesigwriter.writerow([j, timesig])

                if j % 50 == 0: sys.stdout.write('.'); sys.stdout.flush()
                j += 1
            i += 1

        # Close files
        composerfile.close()
        songfile.close()
        keyfile.close()
        timesigfile.close()

        sys.stdout.write(' Done!\n')
        sys.stdout.flush()


if __name__ == "__main__":
    report = Report()
