"""
:Created: 12 March 2015
:Author: Lucas Connors

Tools to fetch data from midiworld.com

:Last worked as expected: 25 March 2015
Note: The code in this file is incredibly delicate and could break
simply if the webpages from midiworld.com are updated by the webmaster

"""

import os
import re
import sys

from bs4 import BeautifulSoup
import requests

from composer import Composer
from song import Song


class LinkNotAMidi(RuntimeError):
    pass


class ComposerMissingInformation(ValueError):
    pass


class MidiWorld(object):

    def __init__(self):
        self.fetch_midis()

    @staticmethod
    def content_before(content, before):
        return content.split(before)[0]

    @staticmethod
    def content_after(content, after):
        return content.split(after)[1]

    @staticmethod
    def composer_info(composer_section):
        death_year = None
        regex_match = re.match(
            r"\s*<font size=\"3\">\s*([^,]+),\s*</font>\s*([^\(]+)\((\d{4})-(\d{4})?\)\s*<i>\s*(\w+)\s*</i>",
            composer_section
        )
        if not regex_match: raise ComposerMissingInformation
        composer_groups = regex_match.groups()

        if len(composer_groups) == 5:
            last_name, first_name, birth_year, death_year, nationality = composer_groups
        else:
            last_name, first_name, birth_year, nationality = composer_groups

        return Composer(
            first_name=first_name,
            last_name=last_name,
            birth_year=birth_year,
            death_year=death_year,
            nationality=nationality,
        )

    @staticmethod
    def song_info(song, main_title=''):
        regex_match = re.match(r"<a href=\"(.*.midi?)\">([^<]*)</a>", unicode(song))
        if not regex_match: raise LinkNotAMidi

        song_url, song_title = regex_match.groups()
        if main_title: song_title = main_title + ' ' + song_title
        return Song(title=song_title, url=song_url)

    @staticmethod
    def songs_from_group(group):
        main_title = re.match(r"(<html><body>)?<li>([^<]+(<i>)?[^<]*(</i>)?[^<]*)<.*", unicode(group)).groups()[1]
        songs = group.find_all('a')
        return main_title, songs

    @classmethod
    def get_webpage_content(cls, url):
        # First check if webpage has already been downloaded locally
        local_name = "webpages/{filename}".format(filename=cls.content_after(content=url, after="www.midiworld.com/"))
        if os.path.isfile(local_name):
            with open(local_name, 'rb') as inf:
                return inf.read()

        # If not, fetch the page and save it
        response = requests.get(url).content
        directory = os.path.dirname(local_name)
        if not os.path.exists(directory): os.makedirs(directory)
        with open(local_name, 'wb') as outf:
            outf.write(response)
        # Then return the response
        return response

    @classmethod
    def extract_songs(cls, song_section):
        all_songs_by_composer = []

        for song_group in song_section:
            if isinstance(song_group, tuple):
                main_title, song_list = song_group
                for song in song_list:
                    try:
                        song_object = cls.song_info(song=song, main_title=main_title)
                    except LinkNotAMidi:
                        continue
                    all_songs_by_composer.append(song_object)
            else:
                try:
                    song_object = cls.song_info(song=song_group)
                except LinkNotAMidi:
                    continue
                all_songs_by_composer.append(song_object)

        return all_songs_by_composer

    @classmethod
    def replace_song_page(cls, links):
        # If there is only one link and it is a composer's page,
        # return the links from that page
        if len(links) > 0:
            url_match = re.match(r"(<html><body>)?<a href=\"(\w*.htm)\">.*</a>(</html></body>)?", unicode(links[0]))
            if url_match:
                url = "http://www.midiworld.com/{webpage}".format(webpage=url_match.groups()[1])
                response = cls.get_webpage_content(url)
                full_page = BeautifulSoup(response)
                song_page = cls.content_before(cls.content_after(unicode(full_page), "<div id=\"content\">"), "<div id=\"sidebar\">")
                song_page_sub = song_page.replace("<a href=\"bwv1060.txt\"><IMG SRC=\"px/info.gif\" ALIGN=\"ABSMIDDLE\" BORDER=0 ALT=\"INFO\"></a>", "").replace("<a name=\"e\"</a>", "").replace("<b>", "").replace("</b>", "")
                song_page_sub = re.sub(r"(<li>[^<]+(<i>)?[^<]*(</i>)?)", "<br$$/>\\1", song_page_sub)
                song_page_sections = [BeautifulSoup(piece_group) for piece_group in song_page_sub.split("<br$$/>")[1:]]
                songs = [cls.songs_from_group(group) for group in song_page_sections]
                return songs

        return links

    def fetch_midis(self):
        sys.stdout.write("Fetching data from MIDI World...")
        sys.stdout.flush()

        # First download the webpage from midiworld.com
        url = "http://www.midiworld.com/classic.htm"
        response = self.get_webpage_content(url)
        full_page = BeautifulSoup(response)
        main_content = unicode(full_page.b)

        # Fix mistakes on the page
        main_content = re.sub(r"</ul>\s*<font size=\"3\"> Joplin", "</ul><br><font size=\"3\"> Joplin", main_content)
        main_content = main_content.replace("<a name=\"e\"</a>", "")

        # Then split up the content into sections
        main_content_sub = re.sub(r"<br>\s*(<a name=\"\w\"></a>)?\s*(<!--.*-->)?\s*<font", "<br$$/>\n<font", main_content)
        sections = [BeautifulSoup(section) for section in main_content_sub.split('<br$$/>')]

        # Break up each section
        composer_sections = []
        for section in sections:
            # Fix mistakes in section
            # TODO: Fix Mendelssohn and Erik Satie
            section_sub = re.sub(r"(<b>\n)?(.*)</i>(</b>)?\s*(<ul>|<li>)", "\\2</i><br/>\\4", unicode(section))

            # Get section with information on composer
            composer_section = BeautifulSoup(section_sub.split("<br/>")[0]).renderContents()
            composer_sections.append(composer_section)
        song_sections = [section.find_all('a') for section in sections]

        # Get songs sections from special composers' pages
        song_sections = map(self.replace_song_page, song_sections)

        # Get composers
        self.composers = []
        for composer_section in composer_sections:
            try:
                composer = self.composer_info(composer_section)
            except ComposerMissingInformation:
                continue
            else:
                self.composers.append(composer)

        # Add songs to dict
        self.songs = {}
        for composer, songs_by_composer in zip(self.composers, song_sections):
            self.songs[composer] = self.extract_songs(songs_by_composer)

        sys.stdout.write(" Done!\n")


if __name__ == "__main__":
    mw = MidiWorld()
