"""
:Created: 27 February 2015
:Author: Lucas Connors

Tools to fetch data from the Internet Archive

"""

import csv
import math
import os
import sys

import internetarchive


class IAComposer(object):

    @classmethod
    def decode(cls, string):
        full, first, last = string.split('$')
        return IAComposer(first=first, last=last, full=full)

    def __init__(self, first=None, last=None, full=None):
        if not first or not last: # only full name provided
            self.full = full
            self.first = self.last = None
        else:
            self.first = first
            self.last = last
            if full:
                self.full = full
            else:
                self.full = "{last}, {first}".format(last=last, first=first)

    def encode(self):
        return '$'.join([self.full, self.first or '', self.last or ''])


class InternetArchive(object):

    def __init__(self, num_items=None):
        self.read_metadata_csv()
        self.fetch_metadata(num_items=num_items)

    @staticmethod
    def break_up_full_name(full_name):
        if ', ' in full_name:
            name_lst = full_name.split(', ')
            last_name = name_lst[0]; first_name = ' '.join(name_lst[1:])
            return IAComposer(first=first_name, last=last_name, full=full_name)
        else:
            return IAComposer(first=None, last=None, full=full_name)

    @classmethod
    def update_metadata(cls, metadata):
        new_metadata = {}

        # Copy useful metadata
        whitelist_keys = [('title', 'title'), ('date', 'date'), ('piece-style', 'genre'), ('subject', 'tags'),]
        for old_key, new_key in whitelist_keys:
            try:
                old_value = metadata[old_key].encode("utf-8")
            except KeyError:
                if new_key == 'tags':
                    new_metadata[new_key] = []
                else:
                    new_metadata[new_key] = ''
            except AttributeError:
                new_metadata[new_key] = [t.encode("utf-8") for t in metadata[old_key]]
            else:
                new_metadata[new_key] = old_value

        # Also update special fields
        try:
            creator_from_metadata = metadata['creator']
        except KeyError:
            creator = ''
        else:
            try:
                creator = creator_from_metadata.encode("utf-8")
            except AttributeError:
                # This is a rare case where multiple composers are credited for the work
                # (thus the data is stored as a list)
                # We just take the first composer; not the best way to handle it, but the simplest
                creator = creator_from_metadata[0].encode("utf-8")
        new_metadata['creator'] = cls.break_up_full_name(creator)

        return new_metadata

    def fetch_metadata(self, num_items=None, csvfilename='ia.csv'):
        sys.stdout.write("Fetching data from Internet Archive...")
        sys.stdout.flush()
        imslp_collection = internetarchive.search_items('collection:imslp')

        if len(self.imslp_metadata) < imslp_collection.num_found:
            # Create CSV file if it does not exist, otherwise append
            if os.path.isfile(csvfilename):
                csvfile = open(csvfilename, 'ab')
                csvwriter = csv.writer(csvfile)
            else:
                csvfile = open(csvfilename, 'wb')
                csvwriter = csv.writer(csvfile)

                # Writer header
                csvwriter.writerow(['ID', 'Full Name', 'First Name', 'Last Name', 'Title', 'Date', 'Genre', 'Tags'])

            i = 0
            for item in imslp_collection:
                item_id = item['identifier']
                if item_id in self.imslp_metadata:
                    if i % 250 == 0: sys.stdout.write("x"); sys.stdout.flush()
                else:
                    if i % 50 == 0: sys.stdout.write("."); sys.stdout.flush()

                    # Add item to metadata dict
                    updated_item = self.update_metadata(internetarchive.get_item(item_id).metadata)
                    self.imslp_metadata[item_id] = updated_item

                    # Add the piece id to composers dict
                    updated_item_creator = updated_item['creator']
                    if updated_item_creator not in self.composers:
                        self.composers[updated_item_creator] = []
                    self.composers[updated_item_creator].append(item_id)

                    # Write to cache
                    self.writeline_metadata_csv(csvwriter=csvwriter, key=item_id, val=updated_item)
                i += 1
                if num_items and i == num_items: break

            csvfile.close() # Close CSV file
        else:
            sys.stdout.write("Nothing to fetch.")

        sys.stdout.write(" Done!\n")

    def read_metadata_csv(self, csvfilename='ia.csv'):
        sys.stdout.write("Reading in data from cache...")
        sys.stdout.flush()

        self.composers = {}
        self.imslp_metadata = {}
        try:
            with open(csvfilename, 'rb') as csvfile:
                csvreader = csv.reader(csvfile)

                # Skip header
                csvreader.next()

                # Read data
                for row in csvreader:
                    id_ = row[0]; full_name = row[1]; first_name = row[2]; last_name = row[3]; title = row[4]; date = row[5]; genre = row[6]; tags = row[7:]
                    creator = IAComposer(
                        first=first_name,
                        last=last_name,
                        full=full_name
                    )
                    self.imslp_metadata[id_] = {
                        'creator': creator,
                        'title': title,
                        'date': date,
                        'genre': genre,
                        'tags': tags,
                    }

                    # Add the piece id to composers dict
                    encoded_creator = creator.encode()
                    if creator not in self.composers:
                        self.composers[encoded_creator] = []
                    self.composers[encoded_creator].append(id_)
        except IOError:
            pass

        sys.stdout.write(" Done! Read {num_entries} entries.\n".format(num_entries=len(self.imslp_metadata)))

    def writeline_metadata_csv(self, csvwriter, key, val):
        # ID
        item_lst = [key]

        # Creator (full name, first name, last name)
        creator_key_lst = ['full', 'first', 'last',]
        for i in creator_key_lst:
            try:
                item_lst.append(getattr(val['creator'], i))
            except AttributeError:
                item_lst.append('')

        # Other keys
        key_lst = ['title', 'date', 'genre',]
        for i in key_lst:
            try:
                item_lst.append(val[i])
            except KeyError:
                item_lst.append('')

        # Tags
        try:
            tags_lst = val['tags']
        except KeyError:
            pass
        else:
            item_lst += [i.encode("utf-8") for i in tags_lst]

        # Write row to file
        csvwriter.writerow(item_lst)

    def write_metadata_csv(self, csvfilename='ia.csv'):
        sys.stdout.write("Writing data to cache...")
        sys.stdout.flush()

        with open(csvfilename, 'wb') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ID', 'Full Name', 'First Name', 'Last Name', 'Title', 'Date', 'Genre', 'Tags'])

            # Write data
            for key, value in self.imslp_metadata.iteritems():
                self.writeline_metadata_csv(csvwriter=csvwriter, key=key, val=value)

        sys.stdout.write(" Done!\n")


if __name__ == "__main__":
    ia = InternetArchive(num_items=25)
