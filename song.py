"""
:Created: 26 March 2015
:Author: Lucas Connors

"""

import os
import re
import sys

import requests


class Song(object):

    def __init__(self, title, url):
        self.title = self.clean_title(title)
        self.midi = self.download_midi(url)

    @staticmethod
    def clean_title(title):
        title = re.sub(r"(\w{1,3}(\s|\.)\d{2,5}(:\w\d)?)|(\(\w{2}\d{2}\))", "", title) # remove number comments
        title = re.sub(r"<.*>.*</.*>", "", title) # remove HTML tags
        title = title.replace("&amp;", "&") # replace HTML chars
        title = re.sub(r"\[.*\]", "", title) # remove comments
        # title = re.sub(r"N(o|O)\.?\s?\d*", "", title) # remove No
        # title = re.sub(r"O(p|P)\.?\s?\d*", "", title) # remove Op
        title = re.sub(r"[-,\.\"\'\(\)]", "", title) # remove special characters
        title = re.sub(r"\s+", " ", title).strip() # remove extra whitespace
        title = title.strip() # remove leading whitespace
        return title

    def download_midi(self, url):
        # Determine the filename given the url
        filename = url.split("www.midiworld.com/")[1]

        # Check if Midi is downloaded already
        # If so, read the data in
        if os.path.isfile(filename):
            with open(filename, 'rb') as inf:
                return inf.read()

        # Download Midi from Midi World
        sys.stdout.write('.'); sys.stdout.flush()
        response = requests.get(url)

        # Save Midi locally
        directory = os.path.dirname(filename)
        if not os.path.exists(directory): os.makedirs(directory)
        with open(filename, 'wb') as outf:
            outf.write(response.content)
        return response.content

    def __repr__(self):
        try:
            return "<Song: {title}>".format(title=self.title)
        except UnicodeEncodeError:
            return "<Song: (cannot encode title)>"
