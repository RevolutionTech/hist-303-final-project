class Composer(object):

    def __init__(self, first_name, last_name, birth_year, death_year, nationality):
        self.first_name = first_name.strip()
        self.last_name = last_name
        self.birth_year = birth_year
        self.death_year = death_year
        self.nationality = nationality

    def __repr__(self):
        return "<Composer: {last_name}, {first_name} ({birth_year}-{death_year})>".format(
            last_name=self.last_name,
            first_name=self.first_name,
            birth_year=self.birth_year,
            death_year=self.death_year if self.death_year else '',
        )

    def add_iacomposer(self, iacomposer):
        self.iacomposer = iacomposer
