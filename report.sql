-- All key signatures
select distinct keysig from KeySigs order by keysig;

-- Cross-product of all possible years and key signatures
select * from Years cross join (select distinct keysig from KeySigs order by keysig) ks;

-- Aggregate of key signatures by year
select year, keysig, count(*) from Years y inner join Composers c on y.year between c.byear and c.dyear inner join Songs s on c.composerid=s.composerid inner join KeySigs k on s.songid=k.songid group by year, keysig order by year, keysig;

-- Aggregate of time signatures by year
select year, timesig, count(*) from Years y inner join Composers c on y.year between c.byear and c.dyear inner join Songs s on c.composerid=s.composerid inner join TimeSigs t on s.songid=t.songid group by year, timesig order by year, timesig;
