"""
:Created: 22 March 2015
:Author: Lucas Connors

"""

import re
import sys

from unidecode import unidecode

from ia import IAComposer, InternetArchive
from midiworld import MidiWorld
from song import Song


def myunidecode(string): # Bit of a hack
    try:
        return unidecode(unicode(string))
    except UnicodeDecodeError:
        return unidecode(string)


class Sources(object):

    def __init__(self):
        self.combine_sources()

    @staticmethod
    def add_metadata_from_ia(mwsong, iasong):
        mwsong.iatitle = iasong['title']
        for attr in ['date', 'genre', 'tags']:
            setattr(mwsong, attr, iasong[attr])
        return mwsong

    @classmethod
    def are_same_song(cls, mwsong, iasong):
        mwsong_title = Song.clean_title(myunidecode(mwsong.title))
        iasong_title = Song.clean_title(myunidecode(iasong['title']))
        result = mwsong_title == iasong_title
        # try:
        #     print "{mwsong_title} {equals} {iasong_title}".format(
        #         mwsong_title=mwsong_title,
        #         equals='==' if result else '!=',
        #         iasong_title=iasong_title
        #     )
        # except UnicodeEncodeError:
        #     pass
        return result

    def combine_composers(self):
        sys.stdout.write("Matching composers between data sources...")
        sys.stdout.flush()

        # Match composers between data sources O(NM)
        self.composers = []
        for mwcomposer in self.mw.composers:
            for encoded_iacomposer in self.ia.composers.iterkeys():
                iacomposer = IAComposer.decode(encoded_iacomposer)
                if (myunidecode(mwcomposer.first_name) == myunidecode(iacomposer.first) and myunidecode(mwcomposer.last_name) == myunidecode(iacomposer.last)) \
                    or ("{last}, {first}".format(last=myunidecode(mwcomposer.last_name), first=myunidecode(mwcomposer.first_name)) == myunidecode(iacomposer.full)):
                    mwcomposer.add_iacomposer(iacomposer)
                    self.composers.append(mwcomposer)

        sys.stdout.write(" Done!\n")
        sys.stdout.flush()

    def combine_music(self):
        sys.stdout.write("Matching music pieces between data sources...")
        sys.stdout.flush()

        # Match songs between data sources O(Cnm)
        # where nm will be a small constant on average in most cases
        self.songs = {}
        num_matches = 0
        for composer in self.composers:
            # Get song data from sources
            mwsongs = self.mw.songs[composer]
            iasong_ids = self.ia.composers[composer.iacomposer.encode()]
            iasongs = map(lambda songid: self.ia.imslp_metadata[songid], iasong_ids)

            matching_songs = []
            # Try to match as many pieces for each composer
            for mwsong in mwsongs:
                for iasong in iasongs:
                    if self.are_same_song(mwsong, iasong):
                        mwsong_with_metadata = self.add_metadata_from_ia(mwsong, iasong)
                        matching_songs.append(mwsong_with_metadata)
                        num_matches += 1
                        break

        sys.stdout.write(" Done! Total of {num_matches} matches.\n".format(num_matches=num_matches))
        sys.stdout.flush()

    def combine_sources(self):
        # Collect data from sources
        self.ia = InternetArchive()
        self.mw = MidiWorld()

        # Combine sources
        self.combine_composers()
        self.combine_music()


if __name__ == "__main__":
    sources = Sources()
