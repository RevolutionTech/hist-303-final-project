"""
:Created: 26 March 2015
:Author: Lucas Connors

Read time signatures from a MIDI file

"""

def get_time_signature_info(midi):
    time_signature_index = 0
    ts_cookie = '\xff\x58\x04'

    while True:
        time_signature_index = midi.find(ts_cookie, time_signature_index) # can raise StopIteration
        if time_signature_index == -1: raise StopIteration

        time_signature = midi[time_signature_index+3:time_signature_index+7]
        numerator = ord(time_signature[0])
        denominator_exponent = ord(time_signature[1])
        yield numerator, denominator_exponent
        time_signature_index += 7

def midi_timesigs(midi):
    for numerator, denominator_exponent in get_time_signature_info(midi):
        # Get time signature info
        denominator = 2 ** denominator_exponent
        yield "{numerator}/{denominator}".format(numerator=numerator, denominator=denominator)

    raise StopIteration
