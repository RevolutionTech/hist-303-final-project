"""
:Created: 25 March 2015
:Author: Lucas Connors

Read key signatures from a MIDI file

"""

MAJOR = True
MINOR = False
key_signatures = {
    -7: {
        MAJOR: "Cb Maj",
        MINOR: "Ab Min",
    },
    -6: {
        MAJOR: "Gb Maj",
        MINOR: "Eb Min",
    },
    -5: {
        MAJOR: "Db Maj",
        MINOR: "Bb Min",
    },
    0: {
        MAJOR: "C Maj",
        MINOR: "A Min",
    },
    1: {
        MAJOR: "G Maj",
        MINOR: "E Min",
    },
    2: {
        MAJOR: "D Maj",
        MINOR: "B Min",
    },
    3: {
        MAJOR: "A Maj",
        MINOR: "F# Min",
    },
    4: {
        MAJOR: "E Maj",
        MINOR: "C# Min",
    },
    5: {
        MAJOR: "B Maj",
        MINOR: "G# Min",
    },
    6: {
        MAJOR: "F# Maj",
        MINOR: "D# Min",
    },
    7: {
        MAJOR: "C# Maj",
        MINOR: "A# Min",
    },
    8: {
        MAJOR: "Ab Maj",
        MINOR: "F Min",
    },
    9: {
        MAJOR: "Eb Maj",
        MINOR: "C Min",
    },
    10: {
        MAJOR: "Bb Maj",
        MINOR: "G Min",
    },
    11: {
        MAJOR: "F Maj",
        MINOR: "D Min",
    },
}

def get_key_signature_info(midi):
    key_signature_index = 0
    ks_cookie = '\xff\x59\x02'

    while True:
        key_signature_index = midi.find(ks_cookie, key_signature_index) # can raise StopIteration
        if key_signature_index == -1: raise StopIteration

        key_signature = midi[key_signature_index+3:key_signature_index+5]
        num_sf = ord(key_signature[0]) # Number of sharps and flats
        minor = bool(ord(key_signature[1]))
        yield num_sf, minor
        key_signature_index += 5

def midi_keys(midi):
    for num_sf, minor in get_key_signature_info(midi):
        # Get key signature info
        major = not minor

        # Determine name of key signature from info
        while True:
            try:
                key_signature_name = key_signatures[num_sf][major]
            except KeyError:
                pass
            else:
                break
            num_sf = num_sf % 12

        yield key_signature_name

    raise StopIteration
